package world.skywars.gameapi.api.item;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class TexturedSkullItem {

    // Fundamentals
    private ItemStackBuilder stackBuilder;

    // Meta
    private String owner;

    public TexturedSkullItem(ItemStackBuilder stackBuilder) {
        this.stackBuilder = stackBuilder;
    }

    // Meta
    public TexturedSkullItem withOwner(String ownerName) {
        this.owner = ownerName;
        return this;
    }

    /**
     * Builds a skull from a owner
     *
     * @return ItemStack skull with owner
     */
    public ItemStack buildSkull() {
        // Build the stack first, edit to make sure it's a skull
        ItemStack skull = stackBuilder.buildItemStack();

        // Edit skull meta
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwner(owner);
        skull.setItemMeta(meta);

        // Lastly, return the skull
        return skull;
    }
}

