package world.skywars.gameapi.api.item;

import lombok.RequiredArgsConstructor;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.List;

public class ItemStackBuilder {
    private ItemStack itemStack;

    public ItemStackBuilder(ItemStack itemStack) {
        this.itemStack = itemStack;
    }


    public ItemStackBuilder(Material itemMaterial, Integer itemAmount, byte subId) {
        this.itemStack = new ItemStack(itemMaterial, itemAmount.intValue(), (short) subId);
    }


    public ItemStackBuilder(Material itemMaterial) {
        this.itemStack = new ItemStack(itemMaterial);
    }


    public ItemStackBuilder(Material itemMaterial, Integer itemAmount) {
        this.itemStack = new ItemStack(itemMaterial, itemAmount.intValue());
    }



    public ItemStackBuilder(Integer itemId, Integer itemAmount, byte subId) {
        this.itemStack = new ItemStack(itemId.intValue(), itemAmount.intValue(), (short) subId);
    }


    public ItemStackBuilder clone() {
        return new ItemStackBuilder(this.itemStack.clone());
    }


    public ItemStackBuilder setDisplayName(String displayName) {
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        itemMeta.setDisplayName(displayName);
        this.itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemStackBuilder setLore(List<String> itemLore) {
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        itemMeta.setLore(itemLore);
        this.itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemStackBuilder setLore(String... itemLore) {
        setLore(Arrays.asList(itemLore));
        return this;
    }

    public ItemStackBuilder clearItemLore() {
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        itemMeta.getLore().clear();
        this.itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemStackBuilder removeLoreLine(String line) {
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        List<String> lore = itemMeta.getLore();

        if (! lore.contains(line)) {
            return this;
        }
        lore.remove(line);
        itemMeta.setLore(lore);
        this.itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemStackBuilder addUnsafeEnchantment(Enchantment enchantment, Integer level) {
        this.itemStack.addUnsafeEnchantment(enchantment, level.intValue());
        return this;
    }

    public ItemStackBuilder removeEnchantment(Enchantment enchantment) {
        this.itemStack.removeEnchantment(enchantment);
        return this;
    }

    public ItemStackBuilder setSubId(byte subId) {
        this.itemStack.setDurability((short) subId);
        return this;
    }

    public ItemStackBuilder setDyeColor(DyeColor color) {
        this.itemStack.setDurability((short) color.getData());
        return this;
    }

    public ItemStackBuilder addItemFlags(ItemFlag... itemFlags) {
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        itemMeta.addItemFlags(itemFlags);
        this.itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemStackBuilder removeItemFlags(ItemFlag... itemFlags) {
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        itemMeta.removeItemFlags(itemFlags);
        this.itemStack.setItemMeta(itemMeta);
        return this;
    }


    @Deprecated
    public ItemStackBuilder setWoolColor(DyeColor color) {
        if (! this.itemStack.getType().equals(Material.WOOL)) {
            return this;
        }
        this.itemStack.setDurability((short) color.getData());
        return this;
    }

    public ItemStackBuilder setLeatherArmorColor(Color color) {
        LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) this.itemStack.getItemMeta();
        leatherArmorMeta.setColor(color);
        this.itemStack.setItemMeta(leatherArmorMeta);
        return this;
    }

    public ItemStackBuilder setSkullOwner(String skullOwner) {
        SkullMeta skullMeta = (SkullMeta) this.itemStack.getItemMeta();
        skullMeta.setOwner(skullOwner);
        this.itemStack.setItemMeta(skullMeta);
        return this;
    }


    public ItemStack getItemStack() {
        return this.itemStack;
    }


    public ItemStack toItemStack() {
        return this.itemStack;
    }


    public ItemStack build() {
        return this.itemStack;
    }


    public ItemStack buildItemStack() {
        return this.itemStack;
    }
}
