package world.skywars.gameapi.api.player;

import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import world.skywars.gameapi.config.GameConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class PlayerFactory {

    private final List<GamePlayer> gamePlayers = new ArrayList<>();

    public GamePlayer parsePlayer(Player player) {
        GamePlayer result = gamePlayers.stream()
                .filter(gamePlayer -> gamePlayer.getPlayer().getUniqueId().equals(player.getUniqueId()))
                .findFirst()
                .orElse(null);

        if (result != null) {
            return result;
        }

        this.loadGamePlayer(player);

        return parsePlayer(player);
    }

    public void loadGamePlayer(Player player) {
        GamePlayer gamePlayer = new GamePlayer(player, null, false);
        this.gamePlayers.add(gamePlayer);
    }

    public List<GamePlayer> getLivingPlayers() {
        return Bukkit.getOnlinePlayers()
                .stream()
                .map(this::parsePlayer)
                .filter(player -> ! (player.isSpectator()))
                .collect(Collectors.toList());
    }

    public List<GamePlayer> getSpectator() {
        return Bukkit.getOnlinePlayers()
                .stream()
                .map(this::parsePlayer)
                .filter(GamePlayer::isSpectator)
                .collect(Collectors.toList());
    }

}

