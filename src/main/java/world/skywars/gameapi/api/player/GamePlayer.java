package world.skywars.gameapi.api.player;

import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import world.skywars.gameapi.api.gamesettings.events.PlayerJoinSpectatorMode;
import world.skywars.gameapi.api.gamesettings.events.PlayerLeaveSpectatorMode;
import world.skywars.gameapi.api.team.GameTeam;

@Data
public class GamePlayer {

    private final Player player;
    private GameTeam team;

    private boolean spectator;

    public GamePlayer(Player player, GameTeam team, boolean spectator) {
        this.player = player;
        this.team = team;
        this.spectator = spectator;
    }

    public void setSpectator(boolean spectator) {
        this.setSpectator(spectator, SpectatorState.NOT_SET);
    }

    public void setSpectator(boolean spectator, SpectatorState spectatorState) {
        this.spectator = spectator;

        if (spectator) {
            Bukkit.getPluginManager().callEvent(new PlayerJoinSpectatorMode(player, spectatorState));
        } else {
            Bukkit.getPluginManager().callEvent(new PlayerLeaveSpectatorMode(player, spectatorState));
        }
    }

    public enum SpectatorState {
        NOT_SET,
        JOIN,
        DISCONNECT;
    }


}

