package world.skywars.gameapi.api.voting;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Vote<T> {

    private UUID creator;
    @Setter
    private T value;

}