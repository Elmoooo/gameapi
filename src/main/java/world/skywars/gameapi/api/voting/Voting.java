package world.skywars.gameapi.api.voting;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class Voting<T> {

    @Getter
    private final List<Vote<T>> votes = new ArrayList<>();

    public void createVote(UUID creator, T t) {
        createVote(new Vote<>(creator, t));
    }

    public void createVote(Vote<T> vote) {
        this.votes.add(vote);
    }

    public Vote<T> getVoteOfPlayer (UUID uuid) {
        return this.votes.stream()
                .filter(vote -> vote.getCreator().equals(uuid))
                .findFirst()
                .orElse(null);
    }

    public void removeVote (UUID creator) {
        final Vote<T> targetVote = getVoteOfPlayer(creator);

        if (targetVote != null) {
            removeVote(targetVote);
        }
    }

    public void removeVote(Vote<T> vote) {
        this.votes.remove(vote);
    }

    public void updateVote (UUID creator, T t) {
        final Vote<T> vote = getVoteOfPlayer(creator);
        vote.setValue(t);
    }

    public int getVotes(T t) {
        return (int) votes.stream()
                .filter(vote -> vote.getValue().equals(t)).count();
    }

    public T getCurrentResult() {
        final Map<T, Integer> voteCounts = getVoteCounts();
        return voteCounts.keySet()
                .stream()
                .max(Comparator.comparingInt(voteCounts::get))
                .orElse(null);
    }

    private Map<T, Integer> getVoteCounts() {
        return getDifferentTypes()
                .stream()
                .collect(Collectors.toMap(type -> type, this::getVotes, (a, b) -> b));
    }

    private List<T> getDifferentTypes() {
        List<T> types = new ArrayList<>();
        this.votes.stream()
                .filter(vote -> ! types.contains(vote.getValue()))
                .forEach(vote -> types.add(vote.getValue()));
        return types;
    }

}
