package world.skywars.gameapi.api.team;

import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import world.skywars.gameapi.api.player.GamePlayer;
import world.skywars.gameapi.api.player.PlayerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class GTeamFactory {

    private final List<GameTeam> teams = new ArrayList<>();
    private final PlayerFactory playerFactory;

    public void addTeam(GameTeam gTeam) {
        this.teams.add(gTeam);
    }

    public void removeTeam(GameTeam gTeam) {
        getTeamMembers(gTeam.getName()).forEach(teamMember -> teamMember.setTeam(null));
        this.teams.remove(gTeam);
    }

    public GameTeam getTeamByName(String teamName) {
        return this.teams.stream()
                .filter(team -> team.getName().equalsIgnoreCase(teamName))
                .findFirst()
                .orElse(null);
    }

    public List<GamePlayer> getTeamMembers(String teamName) {
        return Bukkit.getOnlinePlayers()
                .stream()
                .map(playerFactory::parsePlayer)
                .filter(gamePlayer -> gamePlayer.getTeam() != null && gamePlayer.getTeam().getName().equalsIgnoreCase(teamName))
                .collect(Collectors.toList());
    }

    public List<GameTeam> getTeams() {
        final List<GameTeam> teams = Collections.unmodifiableList(this.teams);
        return teams;
    }
}

