package world.skywars.gameapi.api.team;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.ChatColor;
import org.bukkit.Material;

@Data
@AllArgsConstructor
public class GameTeam {

    private String name;
    private Material material;
    private ChatColor chatColor;

}
