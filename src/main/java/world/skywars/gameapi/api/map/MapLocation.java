package world.skywars.gameapi.api.map;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Location;

@Getter
@RequiredArgsConstructor
public class MapLocation {

    private String path, worldName;
    private double x, y, z;
    private float yaw, pitch;

    public MapLocation(String path, Location location) {
        this.path = path;
        this.worldName = location.getWorld().getName();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.yaw = location.getYaw();
        this.pitch = location.getPitch();
    }

    public Location toLocation() {
        return new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);
    }


}

