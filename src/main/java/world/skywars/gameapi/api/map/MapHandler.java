package world.skywars.gameapi.api.map;

import com.google.gson.GsonBuilder;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.EntityType;
import world.skywars.gameapi.api.gamesettings.events.MapLoadedEvent;
import world.skywars.gameapi.misc.RandomCollector;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

@EqualsAndHashCode
public class MapHandler {

    private final List<GameMap> maps = new ArrayList<>();

    public GameMap getMapByName(String name) {
        AtomicReference<GameMap> result = new AtomicReference<>();
        this.maps.stream()
                .filter(map -> map.getName().equalsIgnoreCase(name))
                .forEach(result::set);
        return result.get();
    }

    public void registerMap(GameMap map) {
        this.maps.add(map);
    }

    @SneakyThrows
    public void loadMap(GameMap map) {
        FileUtils.copyDirectory(new File("maps/" + map.getName()), new File(map.getName()));

        World world = new WorldCreator(map.getName())
                .type(WorldType.FLAT)
                .generator(new SkyGenerator())
                .generateStructures(false)
                .createWorld();

        if (world != null) {
            world.setStorm(false);
            world.setGameRuleValue("doMobSpawning", "false");
            world.setGameRuleValue("doDaylightCycle", "false");
            world.setGameRuleValue("randomTickSpeed", "0");
            world.setGameRuleValue("doFireTick", "false");

            Bukkit.getPluginManager().callEvent(new MapLoadedEvent(map));

            world.getEntities().forEach(entity -> {
                if (! (entity.getType().equals(EntityType.PLAYER))) {
                    entity.remove();
                }
            });
        }

    }


    @SneakyThrows
    public void importMaps() {
        final File file = new File("maps");
        if (file.exists()) {
            for (File files : Objects.requireNonNull(file.listFiles())) {
                if (files.isDirectory()) {
                    final File mapConfig = new File(files, "config.json");

                    if (mapConfig.exists()) {
                        this.registerMap(
                                new GsonBuilder().setPrettyPrinting().create().fromJson(FileUtils.readFileToString(mapConfig, "UTF-8"), GameMap.class)
                        );
                    }
                }
            }
            System.out.println("Registered " + this.maps.size() + " maps.");
        } else {
            System.out.println(file.getName() + "not found!");
        }
    }

    @SneakyThrows
    public void unLoadMap(GameMap map) {
        final World world = Bukkit.getWorld(map.getName());
        if (world != null) {
            world.getPlayers().forEach(players -> players.teleport(Bukkit.getWorlds().get(0).getSpawnLocation()));
            Bukkit.unloadWorld(map.getName(), true);
            FileUtils.deleteDirectory(new File(map.getName()));
        }
    }

    public GameMap findRandomMap() {
        if (this.maps.size() > 0) {
            RandomCollector<GameMap> randomCollector = new RandomCollector<>();
            this.maps.forEach(map -> randomCollector.add(1, map));
            return randomCollector.next();
        }
        return null;
    }

    public List<GameMap> getMaps() {
        return Collections.unmodifiableList(this.maps);
    }

}
