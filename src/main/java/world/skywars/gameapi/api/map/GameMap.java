package world.skywars.gameapi.api.map;


import com.google.common.collect.Lists;
import lombok.Data;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import java.util.List;

@Data
public class GameMap {

    private String name;
    private List<String> creator = Lists.newArrayList();
    private List<MapLocation> spawnLocations = Lists.newArrayList();
    private List<MapLocation> mapLocations = Lists.newArrayList();
    private ItemStack displayItem;

    public Location getLocationByPath(String path) {
        return this.spawnLocations.stream()
                .filter(location -> location.getPath().equalsIgnoreCase(path))
                .findFirst()
                .map(MapLocation::toLocation)
                .orElse(null);
    }

    public Location getMapLocationByPath(String path) {
        return this.mapLocations.stream()
                .filter(location -> location.getPath().equalsIgnoreCase(path))
                .findFirst()
                .map(MapLocation::toLocation)
                .orElse(null);
    }

    public String getBuilderAsString() {
        StringBuilder stringBuilder = new StringBuilder();

        this.creator.forEach(builderName -> {
            if (! stringBuilder.toString().equalsIgnoreCase("")) {
                stringBuilder.append(",");
            }
            stringBuilder.append(builderName);
        });

        return stringBuilder.toString();
    }
}

