package world.skywars.gameapi.api.gamesettings;

public enum GameSettings {

  PVP,
  BUILD,
  BLOCK_PLACE,
  DAMAGE,
  DROP_ITEM,
  NO_FALL_DAMAGE,
  FOOD_LEVEL,
  WEATHER_CHANGE

}
