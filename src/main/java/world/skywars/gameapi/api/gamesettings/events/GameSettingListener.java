package world.skywars.gameapi.api.gamesettings.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import world.skywars.gameapi.api.gamesettings.GameSettings;
import world.skywars.gameapi.phase.PhaseHandler;

public class GameSettingListener implements Listener {

  @EventHandler
  public void onBlockPlaceEvent(BlockPlaceEvent event) {

    if (!PhaseHandler.getInstance().getCurrentPhase().getGameSettingState(GameSettings.BLOCK_PLACE)) {
      event.setCancelled(true);
    }

  }

  @EventHandler
  public void onBlockBreakEvent(BlockBreakEvent event) {

    if (!PhaseHandler.getInstance().getCurrentPhase().getGameSettingState(GameSettings.BUILD)) {
      event.setCancelled(true);
    }

  }

  @EventHandler
  public void onPlayerDamageEvent(EntityDamageEvent event) {

    if (!PhaseHandler.getInstance().getCurrentPhase().getGameSettingState(GameSettings.DAMAGE)) {
      event.setCancelled(true);
      return;
    }

    if (event.getEntity() instanceof Player) {
      if (!PhaseHandler.getInstance().getCurrentPhase().getGameSettingState(GameSettings.PVP)) {
        event.setCancelled(true);
        return;
      }
    }

    if (event.getCause() == DamageCause.FALL) {
      if (PhaseHandler.getInstance().getCurrentPhase().getGameSettingState(GameSettings.NO_FALL_DAMAGE)) {
        event.setDamage(0);
        event.setCancelled(true);
      }
    }

  }

  @EventHandler
  public void onPlayerDropItemEvent(PlayerDropItemEvent event) {

    if (PhaseHandler.getInstance().getCurrentPhase().getGameSettingState(GameSettings.DROP_ITEM)) {
      event.setCancelled(true);
    }

  }

  @EventHandler
  public void onFoodLevelChange(FoodLevelChangeEvent event) {

    if (PhaseHandler.getInstance().getCurrentPhase().getGameSettingState(GameSettings.FOOD_LEVEL)) {
      event.setCancelled(true);
    }

  }

  @EventHandler
  public void onWeatherChangeEvent(WeatherChangeEvent event) {

    if (!PhaseHandler.getInstance().getCurrentPhase().getGameSettingState(GameSettings.WEATHER_CHANGE)) {
      event.setCancelled(true);
    }

  }

}
