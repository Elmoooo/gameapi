package world.skywars.gameapi.api.gamesettings.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import world.skywars.gameapi.api.map.GameMap;

@Getter
@RequiredArgsConstructor
public class MapChangeEvent extends Event {

    private static final HandlerList HANDLER_LIST = new HandlerList();
    private final GameMap to;

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }
}
