package world.skywars.gameapi.api.gamesettings.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import world.skywars.gameapi.misc.GameState;

@Getter
@RequiredArgsConstructor
public class StateChangeEvent extends Event {

    private static final HandlerList HANDLER_LIST = new HandlerList();
    private final GameState from, to;

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }
}
