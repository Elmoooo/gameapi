package world.skywars.gameapi.spectator.listener;


import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.api.gamesettings.events.PlayerJoinSpectatorMode;
import world.skywars.gameapi.api.gamesettings.events.PlayerLeaveSpectatorMode;
import world.skywars.gameapi.api.item.ItemStackBuilder;
import world.skywars.gameapi.api.player.GamePlayer;
import world.skywars.gameapi.misc.GameState;

@RequiredArgsConstructor
public class PlayerListener implements Listener {

    private final GameAPI gameAPI;

    @EventHandler
    public void onJoin(PlayerJoinEvent playerJoinEvent) {
        final Player player = playerJoinEvent.getPlayer();

        if (this.gameAPI.getGameState().equals(GameState.IN_GAME)) {
            final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);
            gamePlayer.setSpectator(true, GamePlayer.SpectatorState.JOIN);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent playerQuitEvent) {
        final Player player = playerQuitEvent.getPlayer();

        if (this.gameAPI.getGameState().equals(GameState.IN_GAME)) {
            final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);
            gamePlayer.setSpectator(false, GamePlayer.SpectatorState.DISCONNECT);
        }
    }

    @EventHandler
    public void onEnterSpectatorMode(PlayerJoinSpectatorMode playerJoinSpectatorMode) {
        final Player player = playerJoinSpectatorMode.getPlayer();

        if (playerJoinSpectatorMode.getState() == GamePlayer.SpectatorState.JOIN) {
            this.loadSpectatorData(player);
        }

        for (Player players : Bukkit.getOnlinePlayers()) {
            final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(players);

            if (gamePlayer.isSpectator()) {
                player.showPlayer(players);
            } else {
                players.hidePlayer(player);
            }
        }
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent playerRespawnEvent) {
        final Player player = playerRespawnEvent.getPlayer();

        final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

        if (gamePlayer.isSpectator()) {
            playerRespawnEvent.setRespawnLocation(player.getLocation());
        }
    }

    @EventHandler
    public void onPostRespawn(PlayerRespawnEvent playerPostRespawnEvent) {
        final Player player = playerPostRespawnEvent.getPlayer();

        final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

        if (gamePlayer.isSpectator()) {
            this.loadSpectatorData(player);
        }
    }

    private void loadSpectatorData(Player player) {
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.getInventory().setItem(0, new ItemStackBuilder(Material.COMPASS).setDisplayName("§eTeleporter").build());
        player.getInventory().setItem(8, new ItemStackBuilder(Material.WATCH).setDisplayName("§cZur Lobby").build());
        player.updateInventory();
        player.setGameMode(GameMode.ADVENTURE);
        player.setAllowFlight(true);
        player.setHealth(20);
        player.setFoodLevel(20);
        player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1000000, 0, true, true));
    }

    @EventHandler
    public void onPotionSplash(PotionSplashEvent potionSplashEvent) {
        Bukkit.getOnlinePlayers().stream()
                .map(player -> this.gameAPI.getPlayerFactory().parsePlayer(player))
                .filter(GamePlayer::isSpectator)
                .forEach(player -> potionSplashEvent.setIntensity(player.getPlayer(), 0.0));
    }

    @EventHandler
    public void onLeaveSpectatorMode(PlayerLeaveSpectatorMode playerLeaveSpectatorMode) {
        final Player player = playerLeaveSpectatorMode.getPlayer();

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.updateInventory();
        player.setGameMode(GameMode.SURVIVAL);
        player.setAllowFlight(false);
        player.removePotionEffect(PotionEffectType.INVISIBILITY);

        for (Player players : Bukkit.getOnlinePlayers()) {
            final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(players);

            if (gamePlayer.isSpectator()) {
                player.hidePlayer(players);
            } else {
                players.showPlayer(player);
            }
        }
    }

    @EventHandler
    public void onPlayerTakeDamage(EntityDamageEvent entityDamageEvent) {
        if (entityDamageEvent.getEntity() instanceof Player) {
            final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer((Player) entityDamageEvent.getEntity());

            if (gamePlayer.isSpectator()) {
                entityDamageEvent.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerGiveDamage(EntityDamageByEntityEvent entityDamageByEntityEvent) {
        if (entityDamageByEntityEvent.getDamager() instanceof Player) {
            final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer((Player) entityDamageByEntityEvent.getDamager());

            if (gamePlayer.isSpectator()) {
                entityDamageByEntityEvent.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerChangeFoodLevel(FoodLevelChangeEvent foodLevelChangeEvent) {
        if (foodLevelChangeEvent.getEntity() instanceof Player) {
            final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer((Player) foodLevelChangeEvent.getEntity());

            if (gamePlayer.isSpectator()) {
                foodLevelChangeEvent.setFoodLevel(20);
                gamePlayer.getPlayer().setSaturation(20);
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent blockBreakEvent) {
        final Player player = blockBreakEvent.getPlayer();

        final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

        if (gamePlayer.isSpectator()) {
            blockBreakEvent.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent blockPlaceEvent) {
        final Player player = blockPlaceEvent.getPlayer();

        final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

        if (gamePlayer.isSpectator()) {
            blockPlaceEvent.setCancelled(true);
        }
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent playerDropItemEvent) {
        final Player player = playerDropItemEvent.getPlayer();

        final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

        if (gamePlayer.isSpectator()) {
            playerDropItemEvent.setCancelled(true);
        }
    }

    @EventHandler
    public void onPickUpItem(PlayerPickupItemEvent playerPickupItemEvent) {
        final Player player = (Player) playerPickupItemEvent.getPlayer();

        final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

        if (gamePlayer.isSpectator()) {
            playerPickupItemEvent.setCancelled(true);
        }

    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getWhoClicked() instanceof Player) {
            Player player = (Player) inventoryClickEvent.getWhoClicked();

            final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

            if (gamePlayer.isSpectator()) {
                inventoryClickEvent.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent playerInteractEvent) {
        final Player player = playerInteractEvent.getPlayer();

        final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

        if (gamePlayer.isSpectator()) {
            playerInteractEvent.setCancelled(true);
        }
    }

    @EventHandler
    public void onChatMessageSend(AsyncPlayerChatEvent asyncPlayerChatEvent) {
        final Player player = asyncPlayerChatEvent.getPlayer();

        final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

        if (gamePlayer.isSpectator()) {
            asyncPlayerChatEvent.setCancelled(true);

            Bukkit.getOnlinePlayers()
                    .stream()
                    .map(players -> this.gameAPI.getPlayerFactory().parsePlayer(players))
                    .filter(GamePlayer::isSpectator)
                    .forEach(players -> players.getPlayer().sendMessage("§c✖ §8» " +
                                                                        ChatColor.translateAlternateColorCodes('&', "&8") + player.getName() + "§8: §f" +
                                                                        asyncPlayerChatEvent.getMessage()));
        }
    }

}

