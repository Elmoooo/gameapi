package world.skywars.gameapi.spectator.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.api.gamesettings.events.PlayerJoinSpectatorMode;
import world.skywars.gameapi.api.item.ItemStackBuilder;
import world.skywars.gameapi.api.item.TexturedSkullItem;
import world.skywars.gameapi.api.player.GamePlayer;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class Navigator implements Listener {

    private final GameAPI gameAPI;
    private final Inventory inventory;

    public Navigator(GameAPI gameAPI) {
        this.gameAPI = gameAPI;
        this.inventory = Bukkit.createInventory(null, 27, "§eSpieler");
        this.fillInventory();
    }

    private void fillInventory() {
        this.inventory.clear();
        AtomicInteger slot = new AtomicInteger(0);

        Bukkit.getOnlinePlayers()
                .stream()
                .map(this.gameAPI.getPlayerFactory()::parsePlayer)
                .filter(players -> ! (players.isSpectator()))
                .forEach(players -> {
                    final ItemStackBuilder itemStackBuilder = new ItemStackBuilder(Material.SKULL_ITEM, 1, (byte) 3)
                            .setDisplayName("§c" + players.getPlayer().getDisplayName());
                    inventory.setItem(slot.get(), new TexturedSkullItem(itemStackBuilder)
                            .withOwner(players.getPlayer().getDisplayName()).buildSkull());
                    slot.set(slot.get() + 1);
                });
    }

    @EventHandler
    public void onInventoryOpen(PlayerInteractEvent playerInteractEvent) {
        final Player player = playerInteractEvent.getPlayer();

        if (playerInteractEvent.getAction() == Action.RIGHT_CLICK_AIR || playerInteractEvent.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (playerInteractEvent.getItem() != null && playerInteractEvent.getItem().getType() == Material.COMPASS) {
                final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

                if (gamePlayer.isSpectator()) {
                    player.openInventory(this.inventory);
                }
            }
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent inventoryClickEvent) {
        final Player player = (Player) inventoryClickEvent.getWhoClicked();

        if (inventoryClickEvent.getClickedInventory() != null && inventoryClickEvent.getClickedInventory().equals(inventory)) {
            final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);

            if (gamePlayer.isSpectator() && inventoryClickEvent.getCurrentItem() != null) {
                inventoryClickEvent.setCancelled(true);

                if (Objects.requireNonNull(inventoryClickEvent.getCurrentItem()).getType() == Material.SKULL_ITEM) {
                    final Player targetPlayer = Bukkit.getPlayer(ChatColor.stripColor(inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName()).split(" ")[0]);

                    if (targetPlayer != null) {
                        player.teleport(targetPlayer.getLocation());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onSpectatorModeJoin(PlayerJoinSpectatorMode playerJoinSpectatorMode) {
        this.fillInventory();
    }

    @EventHandler
    public void onSpectatorModeLeave(PlayerJoinSpectatorMode playerJoinSpectatorMode) {
        this.fillInventory();
    }
}

