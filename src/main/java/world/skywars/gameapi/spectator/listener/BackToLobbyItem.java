package world.skywars.gameapi.spectator.listener;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import world.skywars.gameapi.GameAPI;

@RequiredArgsConstructor
public class BackToLobbyItem implements Listener {

    private final GameAPI gameAPI;

    @EventHandler
    public void onItemInteract(PlayerInteractEvent playerInteractEvent) {
        final Player player = playerInteractEvent.getPlayer();
        if (playerInteractEvent.getAction() == Action.RIGHT_CLICK_AIR || playerInteractEvent.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (playerInteractEvent.getItem() != null) {
                if (playerInteractEvent.getItem().getType() == Material.WATCH) {
                    val gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);
                    if (gamePlayer.isSpectator()) {
                        player.kickPlayer(this.gameAPI.getPrefix() + "§7Du wirst auf die Lobby gesendet...");
                    }
                }
            }
        }
    }

}
