package world.skywars.gameapi.config;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.config.utils.Config;

import java.util.function.Supplier;

@Data
@RequiredArgsConstructor
public class DatabaseConfig implements Config {

    @SerializedName(value = "hostname")
    private String host = "127.0.0.1";

    @SerializedName(value = "authDB")
    private String authDB = "admin";

    @SerializedName(value = "username")
    private String username = "admin";

    @SerializedName(value = "password")
    private String password = "admin123";

    @SerializedName(value = "databaseName")
    private String databaseName = "gamemodeDB";

    @SerializedName(value = "port")
    private int port = 27017;

    public static final Supplier<String> DEFAULT_JSON = () ->
            new GsonBuilder()
                    .setPrettyPrinting()
                    .create()
                    .toJson(new DatabaseConfig());
}
