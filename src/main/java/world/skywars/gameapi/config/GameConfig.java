package world.skywars.gameapi.config;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import world.skywars.gameapi.api.map.MapLocation;
import world.skywars.gameapi.config.utils.Config;

import java.util.function.Supplier;

@Data
public class GameConfig implements Config {

    @SerializedName(value = "gamemodeName")
    private String name = "SkyWars";

    @SerializedName(value = "gameID")
    private String gameID = "SW";

    @SerializedName(value = "prefix")
    private String prefix = "&8[&6SkyWars&8]";

    @SerializedName(value = "requiredPlayersToStart")
    private int requiredPlayersToStart = 2;

    @SerializedName(value = "maxPlayers")
    private int maxPlayers = 10;

    @SerializedName(value = "teamSize")
    private int teamSize = 1;


    @SerializedName(value = "spawnLocation")
    private MapLocation location = new MapLocation("spawn", new Location(Bukkit.getWorld("world"), 1, 1, 1, 1, 1));


    public static final Supplier<String> DEFAULT_JSON = () ->
            new GsonBuilder()
                    .setPrettyPrinting()
                    .create()
                    .toJson(new GameConfig());


}
