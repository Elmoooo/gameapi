package world.skywars.gameapi.config.utils;

import com.google.gson.GsonBuilder;
import lombok.SneakyThrows;
import world.skywars.gameapi.GameAPI;

import java.lang.reflect.Constructor;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.function.Supplier;

public class ConfigReader {

    @SneakyThrows
    public static <T extends Config> T read(String ident, Class<T> clazz, Supplier<String> defaultJson) {
        Path configPath = Paths.get("plugins", "GameProvider", "config", "GameAPI", ident + ".json");

        if (! Files.exists(configPath)) {
            Files.createDirectories(configPath.getParent());
            Files.write(configPath, defaultJson.get().getBytes(), StandardOpenOption.CREATE);

            Constructor<? extends Config> defaultConstructor = clazz.getDeclaredConstructor();
            return (T) defaultConstructor.newInstance();
        }

        return new GsonBuilder().setPrettyPrinting().create()
                .fromJson(Files.newBufferedReader(configPath, StandardCharsets.UTF_8), clazz);
    }

}
