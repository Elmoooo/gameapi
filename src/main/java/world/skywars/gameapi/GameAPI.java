package world.skywars.gameapi;

import com.google.common.collect.Maps;
import com.mongodb.client.MongoCollection;
import java.util.Map;

import lombok.*;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import world.skywars.gameapi.api.gamesettings.events.GameSettingListener;
import world.skywars.gameapi.api.gamesettings.events.StateChangeEvent;
import world.skywars.gameapi.api.map.GameMap;
import world.skywars.gameapi.api.map.MapHandler;
import world.skywars.gameapi.api.player.PlayerFactory;
import world.skywars.gameapi.api.team.GTeamFactory;
import world.skywars.gameapi.config.DatabaseConfig;
import world.skywars.gameapi.config.GameConfig;
import world.skywars.gameapi.database.MongoDBConnector;
import world.skywars.gameapi.misc.GameState;
import world.skywars.gameapi.misc.InventoryUtils;
import world.skywars.gameapi.phase.PhaseHandler;
import world.skywars.gameapi.phase.lobby.command.ForceMapCommand;
import world.skywars.gameapi.phase.lobby.command.StartCommand;
import world.skywars.gameapi.phase.lobby.countdown.LobbyCountdown;
import world.skywars.gameapi.phase.lobby.listener.PlayerJoinListener;
import world.skywars.gameapi.phase.restart.listener.StateChangeListener;
import world.skywars.gameapi.spectator.listener.BackToLobbyItem;
import world.skywars.gameapi.spectator.listener.Navigator;
import world.skywars.gameapi.spectator.listener.PlayerListener;
import world.sykwars.modularity.gameprovider.SpigotModule;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class GameAPI {

  @Getter
  @Setter
  public static GameAPI instance;
  private final Map<Class, MongoCollection<Document>> classToCollectionMap = Maps.newHashMap();
  //phase
  private MongoDBConnector mongoDBConnector;
  private GameConfig gameConfig;
  private DatabaseConfig databaseConfig;
  private PhaseHandler phaseHandler;
  private GameState gameState;
  private MapHandler mapHandler;
  private GTeamFactory gTeamFactory;
  private PlayerFactory playerFactory;
  private Location spawnLocation;
  private SpigotModule spigotModule;
  private GameMap currentMap;
  private LobbyCountdown lobbyCountdown;

  public static GameAPI init(GameConfig gameConfig, DatabaseConfig databaseConfig, SpigotModule spigotModule) {
    final PlayerFactory playerFactory = new PlayerFactory();
    final GTeamFactory gTeamFactory = new GTeamFactory(playerFactory);
    final MapHandler mapHandler = new MapHandler();
    final PhaseHandler phaseHandler = new PhaseHandler();
    final MongoDBConnector tempMongoDBConnector = new MongoDBConnector();

    return new GameAPI(tempMongoDBConnector,
        gameConfig,
        databaseConfig,
        phaseHandler,
        GameState.LOBBY,
        mapHandler,
        gTeamFactory,
        playerFactory,
        gameConfig.getLocation().toLocation(),
        spigotModule,
        null, null);
  }

  public void subInit(GameAPI gameAPI) {
    this.mapHandler.importMaps();
    this.currentMap = this.mapHandler.findRandomMap();
    this.mapHandler.loadMap(currentMap);

    //Events
    this.spigotModule.registerListener(new GameSettingListener());
    this.spigotModule.registerListener(new PlayerJoinListener());
    this.spigotModule.registerListener(new InventoryUtils());
    this.spigotModule.registerListener(new StateChangeListener(gameAPI));
    this.spigotModule.registerListener(new BackToLobbyItem(gameAPI));
    this.spigotModule.registerListener(new Navigator(gameAPI));
    this.spigotModule.registerListener(new PlayerListener(gameAPI));

    //Commands

    this.spigotModule.registerCommand(new StartCommand());
    this.spigotModule.registerCommand(new ForceMapCommand());

    this.connectToDatabase();
  }

  private void connectToDatabase() {
    String[] vars = {this.databaseConfig.getHost(),
        this.databaseConfig.getAuthDB(),
        this.databaseConfig.getUsername(),
        this.databaseConfig.getPassword(),
        this.databaseConfig.getDatabaseName()};
    this.mongoDBConnector.connect(vars);
  }

  public MongoCollection getCollection(Class clazz) {
    return this.classToCollectionMap.get(clazz);
  }

  public void setGameState(GameState gameState) {
    Bukkit.getPluginManager().callEvent(new StateChangeEvent(this.gameState, gameState));
    this.gameState = gameState;
  }
  public GameAPI mapClassToCollection(Class clazz, MongoCollection<Document> collection) {
    this.classToCollectionMap.put(clazz, collection);
    return this;
  }

  public String getPrefix() {
    return this.gameConfig.getPrefix().replace("&", "§");
  }
}


