package world.skywars.gameapi.database;

import com.google.common.collect.Maps;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import world.skywars.gameapi.GameAPI;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;


public class DatabaseIndex {

    private final String id;
    private final Map<String, Object> data;


    protected DatabaseIndex(String id) {
        this.id = id;
        this.data = Maps.newHashMap();

        load();
    }

    public static <T extends DatabaseIndex> T of(String id) {
        return (T) new DatabaseIndex(id);
    }

    public void load() {
        if (this.id == null)
            return;
        if (! exists()) {
            return;
        }
        Optional.ofNullable((Document) GameAPI.getInstance().getCollection(getClass())
                .find(Filters.eq("_id", this.id))
                .first())
                .ifPresent(document -> {
                    Objects.requireNonNull(this.data);
                    document.forEach(this.data::put);
                });
    }

    public void save() {
        Document document = new Document("_id", this.id);

        Objects.requireNonNull(this.data);
        this.data.computeIfPresent(this.id, this.data::remove);
        this.data.remove("_id");
        Objects.requireNonNull(document);
        this.data.forEach(document::append);

        if (exists()) {
            Document updateDocument = new Document("$set", document);
            GameAPI.getInstance().getCollection(getClass()).updateOne(Filters.eq("_id", this.id), updateDocument);
        } else {
            GameAPI.getInstance().getCollection(getClass()).insertOne(document);
        }
    }


    public void delete() {
        GameAPI.getInstance().getCollection(getClass()).deleteOne(Filters.eq("_id", this.id));
    }

    public boolean exists() {
        return (this.id != null && GameAPI.getInstance().getCollection(getClass()).count(Filters.eq("_id", this.id)) != 0L);
    }


    public Optional<?> get(String key) {
        return Optional.ofNullable(this.data.get(key));
    }


    public Object get(String key, Object defaultValue) {
        return this.data.getOrDefault(key, defaultValue);
    }


    public DatabaseIndex set(String key, Object value) {
        if (this.data.containsKey(key)) {
            this.data.replace(key, value);
        } else {
            this.data.put(key, value);
        }
        return this;
    }
}
