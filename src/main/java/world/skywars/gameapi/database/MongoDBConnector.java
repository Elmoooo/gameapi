package world.skywars.gameapi.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import org.bukkit.Bukkit;

public class MongoDBConnector {

  private final ExecutorService executorService = Executors.newCachedThreadPool();
  @Getter
  private MongoClient mongoClient;
  @Getter
  private MongoDatabase database;

  public void connect(String[] vars) {

    final String mongo_host = vars[0];
    final String mongo_auth_db = vars[1];
    final String mongo_user = vars[2];
    final String mongo_pass = vars[3];

    Logger.getLogger("org.mongodb.driver").setLevel(Level.SEVERE);

    FutureTask<MongoClient> task = new FutureTask<>(() -> {
      final MongoCredential credential = MongoCredential.createCredential(mongo_user, mongo_auth_db, mongo_pass.toCharArray());
      final MongoClientOptions options = MongoClientOptions.builder().sslEnabled(false).connectionsPerHost(2000).build();
      return new MongoClient(new ServerAddress(mongo_host), credential, options);
    });

    this.executorService.execute(task);
    try {
      this.mongoClient = task.get();
      Bukkit.getConsoleSender().sendMessage("§aMongoDB has been connected");
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
    this.database = mongoClient.getDatabase(vars[4]);

  }

  public void disconnect() {

    this.mongoClient.close();
    Bukkit.getConsoleSender().sendMessage("§aMongoDB has been disconnected");

  }

}
