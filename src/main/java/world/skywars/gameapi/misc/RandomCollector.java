package world.skywars.gameapi.misc;

import lombok.RequiredArgsConstructor;

import java.security.SecureRandom;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

@RequiredArgsConstructor
public class RandomCollector<T> {

    private final NavigableMap<Double, T> collection = new TreeMap<>();
    private final Random random = new SecureRandom();
    private double total = 0.0;

    public void add(double weight, T t) {
        if (weight > 0) {
            total += weight;
            collection.put(total, t);
        }
    }

    public T next() {
        double value = random.nextDouble() * total;
        return collection.ceilingEntry(value).getValue();
    }

}
