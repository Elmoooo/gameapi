package world.skywars.gameapi.misc;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class DynamicInventory {

    private final String title;
    private final Map<Integer, ItemStack> content = new HashMap<>();

    public DynamicInventory(String title) {
        this.title = title;
    }

    public void setItem(int slot, ItemStack itemStack) {
        this.content.put(slot, itemStack);
    }

    public Inventory createBukkitInventory() {
        final int maxSlot = content.keySet().stream().mapToInt(slot -> slot).max().orElse(0);

        Inventory inventory;

        if (maxSlot <= 9) {
            inventory = Bukkit.createInventory(null, 9, title);
        } else if (maxSlot <= 18) {
            inventory = Bukkit.createInventory(null, 18, title);
        } else if (maxSlot <= 27) {
            inventory = Bukkit.createInventory(null, 27, title);
        } else if (maxSlot <= 36) {
            inventory = Bukkit.createInventory(null, 36, title);
        } else if (maxSlot <= 45) {
            inventory = Bukkit.createInventory(null, 45, title);
        } else if (maxSlot <= 54) {
            inventory = Bukkit.createInventory(null, 54, title);
        } else {
            throw new UnsupportedOperationException("Max inventory size!");
        }

        this.content.keySet().forEach(slot -> inventory.setItem(slot, this.content.get(slot)));
        this.content.clear();

        return inventory;
    }

}

