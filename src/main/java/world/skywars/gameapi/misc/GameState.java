package world.skywars.gameapi.misc;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum GameState {
    LOBBY,
    PEACE_TIME,
    IN_GAME,
    DEATH_MATCH,
    GAME_END
}
