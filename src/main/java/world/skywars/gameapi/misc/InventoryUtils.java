package world.skywars.gameapi.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.IntStream;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryUtils implements Listener {

  private static final Map<Inventory, Consumer<InventoryClickEvent>> events = new HashMap<>();


  public static void fillInventory(Inventory inventory, ItemStack itemStack) {
    IntStream.range(0, inventory.getSize()).forEach(i -> {
      if (inventory.getItem(i) == null) {
        inventory.setItem(i, itemStack);
      } else {
        Objects.requireNonNull(inventory.getItem(i)).getType();
      }
    });
  }

  public static void receiveClickEvent(Inventory inventory, Consumer<InventoryClickEvent> consumer) {
    events.put(inventory, consumer);
  }

  public static void deleteInventory(Inventory inventory) {
    events.remove(inventory);
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent inventoryClickEvent) {
    if (inventoryClickEvent.getClickedInventory() != null) {
      final Inventory clickedInventory = inventoryClickEvent.getClickedInventory();

      if (events.containsKey(clickedInventory)) {
        events.get(clickedInventory).accept(inventoryClickEvent);
      }
    }
  }

}
