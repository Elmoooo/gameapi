package world.skywars.gameapi.phase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.api.gamesettings.GameSettings;
import world.skywars.gameapi.phase.lobby.countdown.LobbyCountdown;

@Getter
@Setter
public abstract class Phase {

  private final Map<Integer, ItemStack> defaultItems = new HashMap<>();
  private final Map<GameSettings, Boolean> gameSettingsMap = new HashMap<>();
  private Location currentSpawnLocation;


  public abstract void onEnable();


  public abstract void onDisable();


  /**
   * The target players inventory will be removed all items, set gamemode survival, clear exp, set food and health level to 20 and clear all actives potions effects.
   *
   * @param player target player
   */
  public void setUpPlayer(Player player) {
    // reset the player values
    player.getInventory().clear();
    player.setHealth(20);
    player.setFoodLevel(20);
    player.setGameMode(GameMode.SURVIVAL);
    player.setExp(0);
    player.getActivePotionEffects().clear();
    // sets the default inventory
    this.defaultItems.keySet().forEach(slotID -> player.getInventory().setItem(slotID, this.defaultItems.get(slotID)));
  }

  /**
   * Adds a default item
   *
   * @param slot      slot id from the inventory
   * @param itemStack itemstack
   */
  public void addDefaultItem(Integer slot, ItemStack itemStack) {
    this.defaultItems.put(slot, itemStack);
  }

  /**
   * Creates a itemstack
   *
   * @param id          Material ID
   * @param subID       Material SubID
   * @param amount      amount
   * @param displayName displayName of the item
   * @param lore        additional text
   * @return new itemstack
   */
  public ItemStack getItemStack(int id, int subID, int amount, String displayName, String... lore) {

    ItemStack itemStack = new ItemStack(id, amount, (short) subID);
    ItemMeta itemMeta = itemStack.getItemMeta();
    itemMeta.setDisplayName(displayName);
    itemMeta.setLore(Arrays.asList(lore));
    itemStack.setItemMeta(itemMeta);
    return itemStack;

  }

  /**
   * Register a listener
   *
   * @param listener listener
   */
  public void registerListener(Listener listener) {
    Bukkit.getPluginManager().registerEvents(listener, GameAPI.getInstance().getSpigotModule().getGameProvider());
  }

  /**
   * Unregister all actives listeners
   */
  public void unRegisterAllListener() {
    HandlerList.unregisterAll();
  }

  /**
   * Sets a gamesetting eg GameSetting.Build and false
   *
   * @param gameSetting GameSettingType
   * @param state       State
   */
  public void setGameSetting(GameSettings gameSetting, boolean state) {
    this.gameSettingsMap.put(gameSetting, state);
  }

  /**
   * Gets a from the gamesetting a current state
   *
   * @param gameSettings GameSetting type
   * @return GameSetting state
   */
  public boolean getGameSettingState(GameSettings gameSettings) {
    if (this.gameSettingsMap.containsKey(gameSettings)) {
      return this.gameSettingsMap.get(gameSettings);
    }
    return false;
  }


}
