package world.skywars.gameapi.phase.lobby.listener;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.api.player.GamePlayer;
import world.skywars.gameapi.misc.GameState;
import world.skywars.gameapi.phase.lobby.event.LobbyItemLoadEvent;
import world.skywars.gameapi.phase.lobby.event.LobbyScoreboardFillEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void handlePlayerJoinEvent(PlayerJoinEvent playerJoinEvent) {
        final Player player = playerJoinEvent.getPlayer();
        final GamePlayer gamePlayer = GameAPI.getInstance().getPlayerFactory().parsePlayer(player);

        try {
            player.teleport(GameAPI.getInstance().getSpawnLocation());
        } catch (NullPointerException e) {
            System.out.println(e);
        }


        if (GameAPI.getInstance().getGameState().equals(GameState.LOBBY)) {


            if (Bukkit.getOnlinePlayers().size() == GameAPI.getInstance().getGameConfig().getMaxPlayers()
                && GameAPI.getInstance().getLobbyCountdown().getCurrentTime() > 10) {
                GameAPI.getInstance().getLobbyCountdown().setCurrentTime(10);
            }

            player.setGameMode(GameMode.ADVENTURE);
            player.setAllowFlight(false);
            player.setFoodLevel(20);
            player.setSaturation(20);
            player.setLevel(0);
            player.setExp(0);

            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
            Bukkit.getPluginManager().callEvent(new LobbyItemLoadEvent(player));

           /* final Scoreboard scoreboard = player.getScoreboard();

            final Objective objective = scoreboard.registerNewObjective(player.getName(), "bbb");
            objective.setDisplayName("§8» §b§lSky§f§lWars §8«");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);

            final LobbyScoreboardFillEvent lobbyScoreboardFillEvent = new LobbyScoreboardFillEvent(player, objective);
            Bukkit.getPluginManager().callEvent(lobbyScoreboardFillEvent);

            player.setScoreboard(scoreboard);
*/
            playerJoinEvent.setJoinMessage(GameAPI.getInstance().getPrefix() + player.getDisplayName() + "§7 hat den Server betreten.");
        }
    }
}
