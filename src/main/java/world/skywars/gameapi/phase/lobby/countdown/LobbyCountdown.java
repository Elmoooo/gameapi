package world.skywars.gameapi.phase.lobby.countdown;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.scheduler.BukkitRunnable;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.api.gamesettings.events.GameStartEvent;
import world.skywars.gameapi.config.GameConfig;
import world.skywars.gameapi.misc.GameState;
import world.skywars.gameapi.phase.lobby.event.LobbyCountdownTickEvent;

public class LobbyCountdown extends BukkitRunnable {

    private final int startedTime;
    private final GameConfig gameConfig;
    @Getter
    @Setter
    private int currentTime;

    public LobbyCountdown(int time, GameConfig gameConfig) {
        this.startedTime = time;
        this.currentTime = time;
        this.gameConfig = gameConfig;
    }

    @Override
    public void run() {
        if (Bukkit.getOnlinePlayers().size() >= gameConfig.getRequiredPlayersToStart()) {
            Bukkit.getPluginManager().callEvent(new LobbyCountdownTickEvent(currentTime));
            switch (currentTime) {
                case 120: case 90: case 60: case 30: case 15:
                    Bukkit.broadcastMessage("");
                    Bukkit.broadcastMessage(GameAPI.getInstance().getPrefix() +
                                            "§7Das Spiel startet in §a" + this.currentTime + "§7 Sekunden.");

                    Bukkit.getOnlinePlayers().forEach(players -> {
                        players.setLevel(this.currentTime);
                        players.setExp((float) this.currentTime / (float) this.startedTime);
                    });
                    break;

                case 10: case 5: case 4: case 3: case 2:
                    Bukkit.broadcastMessage(GameAPI.getInstance().getPrefix() +
                                            "§7Das Spiel startet in §a" + this.currentTime + "§7 Sekunden.");

                    Bukkit.getOnlinePlayers().forEach(players -> {
                        players.setLevel(this.currentTime);
                        players.setExp((float) this.currentTime / (float) this.startedTime);
                    });
                    break;

                case 1:
                    Bukkit.broadcastMessage(GameAPI.getInstance().getPrefix() +
                                            "§7Das Spiel startet in §a einer §7 Sekunde.");
                    Bukkit.getOnlinePlayers().forEach(players -> {
                        players.setLevel(this.currentTime);
                        players.setExp((float) this.currentTime / (float) this.startedTime);
                    });
                    break;

                case 0:

                    Bukkit.broadcastMessage(GameAPI.getInstance().getPrefix() +
                                            "§7Das Spiel wird gestartet...");

                    Bukkit.getOnlinePlayers().forEach(players -> {
                        players.setHealth(20.0);
                        players.setAllowFlight(false);
                        players.setFlying(false);
                        players.setLevel(0);
                        players.setExp(0);
                        players.setGameMode(GameMode.SURVIVAL);
                        players.setFoodLevel(20);
                        players.setSaturation(20);
                        players.setFireTicks(0);
                    });

                    this.cancel();
                    GameAPI.getInstance().setGameState(GameState.PEACE_TIME);
                    GameStartEvent gameStartEvent = new GameStartEvent();
                    Bukkit.getPluginManager().callEvent(gameStartEvent);
                    break;

                default:
                    Bukkit.getOnlinePlayers().forEach(players -> {
                        players.setLevel(this.currentTime);
                        players.setExp((float) this.currentTime / (float) this.startedTime);
                    });
                    break;

            }

            this.currentTime--;
        } else {
            this.currentTime = this.startedTime;
            Bukkit.getOnlinePlayers().forEach(players -> {
                players.setLevel(this.currentTime);
                players.setExp((float) this.currentTime / (float) this.startedTime);
            });
        }
    }
}
