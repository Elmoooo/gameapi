package world.skywars.gameapi.phase.lobby.command;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.phase.lobby.countdown.LobbyCountdown;
import world.sykwars.modularity.gameprovider.command.Command;
import world.sykwars.modularity.gameprovider.command.CommandAnnotation;

@CommandAnnotation(name = "start", permission = "game.start")
public class StartCommand extends Command {

  @Override
  public void run(CommandSender commandSender, String[] strings) {
    if (!(commandSender instanceof Player) || !commandSender.hasPermission("game.start")) {
      return;
    }

    final Player player = (Player) commandSender;
    final LobbyCountdown lobbyCountdown = GameAPI.getInstance()
        .getLobbyCountdown();

    if (lobbyCountdown.getCurrentTime() > 10) {
      if (Bukkit.getOnlinePlayers().size() >= GameAPI.getInstance().getGameConfig().getRequiredPlayersToStart()) {
        lobbyCountdown.setCurrentTime(10);
        player.sendMessage(GameAPI.getInstance().getPrefix() + "§7Das Spiel wird gestartet.");
      } else {
        player.sendMessage(GameAPI.getInstance().getPrefix() + "§cEs fehlen noch Spieler, damit das Spiel starten kann.");
      }
    } else {
      player.sendMessage(GameAPI.getInstance().getPrefix() + "§cDas Spiel wurde bereits gestartet.");
    }

  }
}

