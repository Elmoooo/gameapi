package world.skywars.gameapi.phase.lobby.command;

import java.util.Collections;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.api.gamesettings.events.MapChangeEvent;
import world.skywars.gameapi.api.item.ItemStackBuilder;
import world.skywars.gameapi.api.map.GameMap;
import world.skywars.gameapi.api.map.MapHandler;
import world.skywars.gameapi.misc.DynamicInventory;
import world.skywars.gameapi.misc.GameState;
import world.skywars.gameapi.misc.InventoryUtils;
import world.sykwars.modularity.gameprovider.command.Command;
import world.sykwars.modularity.gameprovider.command.CommandAnnotation;

@CommandAnnotation(name = "forcemap", permission = "game.forcemap")
public class ForceMapCommand extends Command {

  @Override
  public void run(CommandSender commandSender, String[] strings) {

    if (!(commandSender instanceof Player) || !commandSender.hasPermission("game.forcemap")) {
      return;
    }
    final Player player = (Player) commandSender;
    if (strings.length >= 1) {
      final String targetMapName = strings[0];

      GameAPI.getInstance().setGameState(GameState.GAME_END);

      if (GameAPI.getInstance().getLobbyCountdown().getCurrentTime() > 10) {
        final MapHandler mapHandler = GameAPI.getInstance().getMapHandler();
        final GameMap map = mapHandler.getMapByName(targetMapName);

        if (map != null) {
          if (GameAPI.getInstance().getCurrentMap() != null) {
            mapHandler.unLoadMap(map);
          }
          mapHandler.loadMap(map);
          GameAPI.getInstance().setCurrentMap(map);
          Bukkit.getPluginManager().callEvent(new MapChangeEvent(map));
          player.sendMessage(GameAPI.getInstance().getPrefix() + "§7Die Map wurde zu §a" + targetMapName + "§7 geändert.");
          return;
        }
        player.sendMessage(GameAPI.getInstance().getPrefix() + "§cDie Map §e" + targetMapName + "§c wurde nicht gefunden!");
      } else {
        player.sendMessage(GameAPI.getInstance().getPrefix() + "§cDu kannst die Map nicht mehr ändern!");
      }
    } else {
      openMapGui(player);
    }

  }

  private void openMapGui(Player player) {
    DynamicInventory dynamicInventory = new DynamicInventory("§6§lMap auswählen");

    int slot = 0;
    for (GameMap gameMap : GameAPI.getInstance().getMapHandler().getMaps()) {
      Material material = gameMap.getDisplayItem().getType();

      if (material == null) {
        material = Material.MAP;
      }

      final ItemStack itemStack = new ItemStackBuilder(material)
          .setDisplayName("§a" + gameMap.getName())
          .setLore(Collections.singletonList("§7Builder §8» §a" + gameMap.getBuilderAsString()))
          .build();

      dynamicInventory.setItem(slot, itemStack);
      slot++;
    }

    final Inventory inventory = dynamicInventory.createBukkitInventory();
    InventoryUtils.receiveClickEvent(inventory, this::accept);
    player.openInventory(inventory);
  }

  public void accept(InventoryClickEvent inventoryClickEvent) {
    final Player player = (Player) inventoryClickEvent.getWhoClicked();
    inventoryClickEvent.setCancelled(true);

    if (inventoryClickEvent.getCurrentItem() != null
        && inventoryClickEvent.getCurrentItem().hasItemMeta()
        && inventoryClickEvent.getCurrentItem().getItemMeta().hasDisplayName()) {
      inventoryClickEvent.getCurrentItem().getType();
      String mapName = ChatColor.stripColor(inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName());
      player.performCommand("forcemap " + mapName);
      InventoryUtils.deleteInventory(inventoryClickEvent.getClickedInventory());
      player.closeInventory();
    }
  }
}
