package world.skywars.gameapi.phase;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;
import world.skywars.gameapi.phase.lobby.countdown.LobbyCountdown;

@Getter
@Setter
public class PhaseHandler {

  @Getter
  private static PhaseHandler instance;
  private Phase currentPhase;
  private Gson gson = new GsonBuilder().setPrettyPrinting().create();

  public PhaseHandler() {
    instance = this;
  }

  /**
   * Start a new phase
   *
   * @param phase new phase
   */

  /**
   * Start a new phase
   *
   * @param phase new phase
   */
  public void startPhase(Phase phase) {

    if (this.currentPhase != null) {
      this.stopPhase(phase);
    }
    this.currentPhase = phase;
    this.currentPhase.onEnable();

  }

  /**
   * Stops the currently phase
   *
   * @param phase current phase
   */
  public void stopPhase(Phase phase) {
    phase.onDisable();
  }

}
