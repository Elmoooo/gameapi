package world.skywars.gameapi.phase.restart.listener;

import lombok.RequiredArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.api.gamesettings.events.StateChangeEvent;
import world.skywars.gameapi.misc.GameState;
import world.skywars.gameapi.phase.restart.RestartPhase;
import world.skywars.gameapi.phase.restart.countdown.RestartCountdown;

@RequiredArgsConstructor
public class StateChangeListener implements Listener {
    private final GameAPI gameAPI;
    @EventHandler
    public void onStateChange(StateChangeEvent stateChangeEvent) {
        if (stateChangeEvent.getTo() == GameState.GAME_END) {
            new RestartCountdown(gameAPI, 25).runTaskTimer(gameAPI.getSpigotModule().getGameProvider(), 0, 20L);
            GameAPI.getInstance().getPhaseHandler().startPhase(new RestartPhase());
        }
    }

}
