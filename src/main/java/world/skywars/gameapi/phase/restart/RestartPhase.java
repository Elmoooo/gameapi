package world.skywars.gameapi.phase.restart;

import world.skywars.gameapi.api.gamesettings.GameSettings;
import world.skywars.gameapi.phase.Phase;

public class RestartPhase extends Phase {

    @Override
    public void onEnable() {

        //TODO : MORE WITH PHASEHANDLER !!!!!!!!!!!!!! //Elmo
        this.setCurrentSpawnLocation(null);

        //game settings:
        this.setGameSetting(GameSettings.BUILD, false);
        this.setGameSetting(GameSettings.BLOCK_PLACE, false);
        this.setGameSetting(GameSettings.PVP, false);
        this.setGameSetting(GameSettings.DAMAGE, false);
        this.setGameSetting(GameSettings.FOOD_LEVEL, false);
        this.setGameSetting(GameSettings.NO_FALL_DAMAGE, true);
        this.setGameSetting(GameSettings.WEATHER_CHANGE, false);


    }

    @Override
    public void onDisable() {

    }

}
