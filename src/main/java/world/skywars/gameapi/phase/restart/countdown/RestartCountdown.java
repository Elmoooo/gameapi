package world.skywars.gameapi.phase.restart.countdown;

import org.bukkit.*;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import world.skywars.gameapi.GameAPI;
import world.skywars.gameapi.api.gamesettings.events.PlayerWinEvent;
import world.skywars.gameapi.api.player.GamePlayer;
import world.skywars.gameapi.api.team.GameTeam;
import world.skywars.gameapi.config.GameConfig;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RestartCountdown extends BukkitRunnable {

    private final String prefix;
    private final GameConfig gameConfig;
    private final GameAPI gameAPI;

    private int timeToRestart;

    public RestartCountdown(GameAPI gameAPI, int timeToRestart) {
        this.gameAPI = gameAPI;
        this.prefix = gameAPI.getPrefix();
        this.timeToRestart = timeToRestart;
        this.gameConfig = gameAPI.getGameConfig();
    }

    @Override
    public synchronized BukkitTask runTaskTimer(Plugin plugin, long delay, long period)
            throws IllegalArgumentException, IllegalStateException {

        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("");

        boolean foundAnyWinner = false;

        if (gameAPI.getGTeamFactory().getTeams().size() > 0) {
            final List<GameTeam> livingTeams = gameAPI.getGTeamFactory().getTeams()
                    .stream()
                    .filter(team -> gameAPI.getGTeamFactory().getTeamMembers(team.getName()).stream().anyMatch(gamePlayer -> ! (gamePlayer.isSpectator())))
                    .collect(Collectors.toList());

            if (livingTeams.size() == 1) {
                final GameTeam gTeam = livingTeams.get(0);

                Bukkit.broadcastMessage(this.prefix + "§7Das Team " + gTeam.getChatColor() + gTeam.getName() + "§7 hat das Spiel gewonnen!");
                gameAPI.getGTeamFactory().getTeamMembers(gTeam.getName()).forEach(player -> Bukkit.getPluginManager().callEvent(new PlayerWinEvent(player)));
                foundAnyWinner = true;
            }
        } else {
            final List<GamePlayer> livingPlayers = Bukkit.getOnlinePlayers().stream()
                    .map(player -> this.gameAPI.getPlayerFactory().parsePlayer(player))
                    .filter(gamePlayer -> ! (gamePlayer.isSpectator()))
                    .collect(Collectors.toList());

            if (livingPlayers.size() == 1) {
                final GamePlayer gamePlayer = livingPlayers.get(0);
                Bukkit.broadcastMessage(this.prefix + "§7Der Spieler " + gamePlayer.getPlayer().getDisplayName() + "§7 hat das Spiel gewonnen!");
                Bukkit.getPluginManager().callEvent(new PlayerWinEvent(gamePlayer));
                foundAnyWinner = true;
            }
        }

        if (! (foundAnyWinner)) {
            Bukkit.broadcastMessage(this.prefix + "§cDas Spiel wurde vom Administrator beendet!");
        }

        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("");

        return super.runTaskTimer(plugin, delay, period);
    }

    @Override
    public void run() {


        if (this.timeToRestart == 24) {
            final Location spawnLocation = gameAPI.getSpawnLocation();

            for (Player player : Objects.requireNonNull(Bukkit.getWorld(this.gameAPI.getCurrentMap().getName())).getPlayers()) {
                final GamePlayer gamePlayer = this.gameAPI.getPlayerFactory().parsePlayer(player);
                player.teleport(spawnLocation);
                player.setHealth(20.0);
                player.setFoodLevel(20);
                player.setSaturation(20);
                player.getInventory().clear();
                player.getInventory().setArmorContents(null);
                player.setLevel(0);
                player.setExp(0);
                player.updateInventory();
                player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
                player.getScoreboard().clearSlot(DisplaySlot.PLAYER_LIST);
                player.getScoreboard().clearSlot(DisplaySlot.BELOW_NAME);
                player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
                player.setGameMode(GameMode.ADVENTURE);
                gamePlayer.setSpectator(false);
            }
        }

        switch (this.timeToRestart) {
            case 15:
            case 10:
            case 5:
            case 4:
            case 3:
            case 2:
                Bukkit.broadcastMessage(this.prefix + "§cDer Server startet in §e" + timeToRestart + "§c Sekunden neu.");
                break;

            case 1: {
                Bukkit.broadcastMessage(this.prefix + "§cDer Server startet in §e" + timeToRestart + "§c Sekunde neu.");
                break;
            }

            case 0: {
                this.cancel();
                break;
            }
        }

        this.timeToRestart--;
    }

    @Override
    public void cancel() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.kickPlayer(this.prefix + "§7Alle Spieler werden auf die Lobby gesendet.");
        }
        Bukkit.shutdown();
        super.cancel();
    }
}
